#!/usr/bin/python
# coding=utf-8

from mergeRegisData import MergeData
from byCsv import *


def get_what(status):

    """
    获取正在登记、全部、暂未开始、结束登记页面的房源概况和详情
    :param status:
    :return:
    """
    # 正在登记 全部 暂未开始
    context = MergeData().merge_regis_data(status)
    text = ''
    price_csv = './csvData/xm_price.csv'

    for i in range(len(context)):
        if context[0] == context[2] == '0':
            if status == '暂未开始':
                status = '即将登记'
            text = '' + '城六区和长安区%s均暂无项目，详情请查看房管局官网~\n' % status
            text += '\n~~~ 来自后台自动推送'
        else:
            if context[0] == '0':
                if status == '暂未开始':
                    status = '即将登记'
                text = '' + '【%s项目❗❗】\n\n' % status
                text += '城六区%s的暂无项目~\n' % status
                text += '长安区%s项目数：%s\n' % (status, context[2])
                for j in range(3, len(context)):
                    if type(context[j]) is dict:
                        for key, values in context[j].items():
                            if len(values) == 5:
                                text += '\n项目名：%s\n' % key
                                text += '房源：%s\n' % values[0]
                                text += '均价：%s\n' % check_from_csv(price_csv, key)
                                text += '登记时间：%s\n' % values[1]
                                # text += '收资料时间：%s\n' % values[2]
                                text += '地址：%s\n' % values[3]
                                text += '%s还剩：%s\n' % (values[4].split('还剩')[0], values[4].split('还剩')[1])
                            elif len(values) == 4:
                                text += '\n项目名：%s\n' % key
                                text += '房源：%s\n' % values[0]
                                text += '均价：%s\n' % check_from_csv(price_csv, key)
                                text += '登记时间：%s\n' % values[1]
                                # text += '收资料时间：%s\n' % values[2]
                                text += '地址：%s\n' % values[3]
                            elif len(values) == 3:
                                text += '\n项目名：%s\n' % key
                                text += '→ %s\n' % values[0]
                                text += '→ %s\n' % values[1]
                                text += '→ %s\n' % values[2]
                            else:
                                print('len(values) error !')
                    else:
                        print('context[j] not dict !')
                text += '\n~~~ 来自后台自动推送'

            elif context[-2] == '0':
                if status == '暂未开始':
                    status = '即将登记'
                text = '' + '【%s项目❗❗】\n\n' % status
                text += '长安区%s的暂无项目~\n' % status
                text += '城六区%s项目数：%s\n' % (status, context[0])
                for k in range(1, len(context)-2):
                    if type(context[k]):
                        for key, values in context[k].items():
                            if len(values) == 5:
                                text += '\n项目名：%s\n' % key
                                text += '房源：%s\n' % values[0]
                                text += '均价：%s\n' % check_from_csv(price_csv, key)
                                text += '登记时间：%s\n' % values[1]
                                # text += '收资料时间：%s\n' % values[2]
                                text += '地址：%s\n' % values[3]
                                text += '%s还剩：%s\n' % (values[4].split('还剩')[0], values[4].split('还剩')[1])
                            elif len(values) == 4:
                                text += '\n项目名：%s\n' % key
                                text += '房源：%s\n' % values[0]
                                text += '均价：%s\n' % check_from_csv(price_csv, key)
                                text += '登记时间：%s\n' % values[1]
                                # text += '收资料时间：%s\n' % values[2]
                                text += '地址：%s\n' % values[3]
                            elif len(values) == 3:
                                text += '\n项目名：%s\n' % key
                                text += '→ %s\n' % values[0]
                                text += '→ %s\n' % values[1]
                                text += '→ %s\n' % values[2]
                            else:
                                print('len(values) error !')
                    else:
                        print('context[k] not dict !')
                text += '\n~~~ 来自后台自动推送'

            else:
                if status == '暂未开始':
                    status = '即将登记'
                text = '' + '【%s项目❗❗】\n\n' % status
                text += '城六区%s项目数：%s\n' % (status, context[0])
                for m in range(1, len(context)):
                    if type(context[m]) is dict:
                        for key, values in context[m].items():
                            if len(values) == 5:
                                text += '\n项目名：%s\n' % key
                                text += '房源：%s\n' % values[0]
                                text += '均价：%s\n' % check_from_csv(price_csv, key)
                                text += '登记时间：%s\n' % values[1]
                                # text += '收资料时间：%s\n' % values[2]
                                text += '地址：%s\n' % values[3]
                                text += '%s还剩：%s\n' % (values[4].split('还剩')[0], values[4].split('还剩')[1])
                            elif len(values) == 4:
                                text += '\n项目名：%s\n' % key
                                text += '房源：%s\n' % values[0]
                                text += '均价：%s\n' % check_from_csv(price_csv, key)
                                text += '登记时间：%s\n' % values[1]
                                # text += '收资料时间：%s\n' % values[2]
                                text += '地址：%s\n' % values[3]
                            elif len(values) == 3:
                                text += '\n项目名：%s\n' % key
                                text += '→ %s\n' % values[0]
                                text += '→ %s\n' % values[1]
                                text += '→ %s\n' % values[2]
                            else:
                                print('len(values) error !')
                    else:
                        text += '\n长安区%s项目数：%s\n' % (status, context[m])
                text += '\n~~~ 来自后台自动推送'

    return text
