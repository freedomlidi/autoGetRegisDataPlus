from getRegistered import get_all_registered, get_partial_registered
from getWhat import get_what
from wxpy import *


bot = Bot(cache_path=True)
tuling = Tuling(api_key='...*...')

# 正 式 群 聊
# AI_ql = bot.groups().search('西安楼市AI买房交流')
# ZYGX_ql = bot.groups().search('买房资源共享沟通群')[0]

# 测 试 群 聊
ql = bot.groups().search('群聊')


@bot.register(ql[0])
def reply_msg(msg):
    if msg.is_at:
        chat_target = str(msg.member).split(':')[1].split('>')[0].strip()
        self_name = str(msg.receiver).split(':')[1].split('>')[0].strip()
        content = msg.text.split('%s' % self_name)[-1].strip()
        if '正在登记' in content:
            msg.reply('正在查询【正在登记】项目，请稍后~')
            return get_what('正在登记')
        elif '即将登记' in content:
            msg.reply('正在查询【即将登记】项目，请稍后~')
            return get_what('暂未开始')
        elif '全部结束登记' in content:
            msg.reply('正在查询【全部结束登记】项目，输出为csv文件格式，时间较长，请稍后~')
            try:
                get_all_registered()
            except Exception as e:
                print(e)
                msg.reply('抱歉，获取数据失败，稍后请重试~')
            else:
                msg.reply_file('./csvData/registered.csv')
        elif '结束登记' in content:
            msg.reply('正在查询【最新结束登记】项目，请稍后~')
            try:
                msg.reply(get_partial_registered())
            except Exception as e:
                print(e)
                msg.reply('抱歉，获取数据失败，稍后请重试~')
        elif '我要登记' in content:
            msg.reply('房管局登记网站：http://124.115.228.93/zfrgdjpt/index.html')
        elif 'ss' in content or 'Ss' in content or 'sS' in content or 'SS' in content:
            tuling.do_reply(msg)
        else:
            msg.reply('你好，%s！\n\n'
                      '1、@我输入关键字`正在登记`，查询【正在登记】项目\n'
                      '2、@我输入关键字`即将登记`，查询【即将登记】项目\n'
                      '3、@我输入关键字`结束登记`，查询【最新结束登记】项目\n'
                      '4、@我输入关键字`全部结束登记`，查询【全部结束登记】项目\n'
                      '5、@我输入关键字`我要登记`，查询房管局登记网站\n'
                      '6、@我输入`ss`+`要对我说的话`，可以和我聊天，尬聊哦' % chat_target)


@bot.register(ql[1])
def reply_msg_no_gl(msg):
    if msg.is_at:
        chat_target = str(msg.member).split(':')[1].split('>')[0].strip()
        self_name = str(msg.receiver).split(':')[1].split('>')[0].strip()
        content = msg.text.split('%s' % self_name)[-1].strip()
        if '正在登记' in content:
            msg.reply('正在查询【正在登记】项目，请稍后~')
            return get_what('正在登记')
        elif '即将登记' in content:
            msg.reply('正在查询【即将登记】项目，请稍后~')
            return get_what('暂未开始')
        elif '全部结束登记' in content:
            msg.reply('正在查询【全部结束登记】项目，输出为csv文件格式，时间较长，请稍后~')
            try:
                get_all_registered()
            except Exception as e:
                print(e)
                msg.reply('抱歉，获取数据失败，稍后请重试~')
            else:
                msg.reply_file('./csvData/registered.csv')
        elif '结束登记' in content:
            msg.reply('正在查询【最新结束登记】项目，请稍后~')
            try:
                msg.reply(get_partial_registered())
            except Exception as e:
                print(e)
                msg.reply('抱歉，获取数据失败，稍后请重试~')
        elif 'lt' in content or 'gl' in content or 'ss' in content:
            # tuling.do_reply(msg)
            msg.reply('抱歉，此群不允许小助手聊天~')
        elif '我要登记' in content:
            msg.reply('房管局登记网站：http://124.115.228.93/zfrgdjpt/index.html')
        else:
            msg.reply('你好，%s！\n\n'
                      '1、@我输入关键字`正在登记`，查询【正在登记】项目\n'
                      '2、@我输入关键字`即将登记`，查询【即将登记】项目\n'
                      '3、@我输入关键字`结束登记`，查询【最新结束登记】项目\n'
                      '4、@我输入关键字`全部结束登记`，查询【全部结束登记】项目\n'
                      '5、@我输入关键字`我要登记`，查询房管局登记网站' % chat_target)


# embed()
bot.join()
