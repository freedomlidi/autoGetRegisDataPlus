#!/usr/bin/python
# coding=utf-8

import csv


def write_csv(csv_fp, row_list):
    """
    数据写入csv文件
    :return:
    """
    with open(csv_fp, 'w', encoding='utf-8', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        writer.writerow(row_list)


def write_registered(row_list):
    """
    数据写入csv文件
    :return:
    """
    with open('./csvData/registered.csv', 'a', encoding='utf-8-sig', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        writer.writerow(row_list)


def read_csv(csv_fp, row_num):
    """
    读取csv数据，按行读取
    :param csv_fp: 路径 + 文件名
    :param row_num: 行数
    :return:
    """
    with open(csv_fp, 'r', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        for i, rows in enumerate(reader):
            if i == row_num - 1:
                return rows


def add_csv(csv_fp, row_list):
    """
    追加写入csv文件
    :return:
    """
    with open(csv_fp, 'a', encoding='utf-8', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        writer.writerow(row_list)


def check_from_csv(csv_fp, xm_name):
    """
    在csv文件检索匹配为行，并返回对应价格
    :param csv_fp: 路径 + 文件名
    :param xm_name: 项目名
    :return:
    """
    with open(csv_fp, 'r', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        # print(type(reader))
        for i, rows in enumerate(reader):
            if rows[0] == xm_name:
                return rows[-1]


if __name__ == '__main__':
    fp = './csvData/xm_price.csv'
    f = check_from_csv(fp, '[长安区] 郭南村城中村改造项目GN-4地块（海亮·熙悦）（长安区） ')
    print(f)
