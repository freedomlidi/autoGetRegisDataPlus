#!/usr/bin/python
# coding=utf-8

import os
from wxpy import *
import configparser as cparser

cfg_path = './receiverCfg.ini'
cf = cparser.ConfigParser()
cf.read(cfg_path, encoding='utf-8')


def send_by_wechat(type_, name, context=None, img_list=None):
    """
    微信发送数据到个人或者群聊
    :param type_: 个人'friend'，群聊'group'
    :param name: 个人/群聊名称
    :param context: 发送内容 console_qr=2,
    :return:
    """
    bot = Bot(cache_path=True)
    if type_ == 'group':
        my_group = bot.groups().search(name)[0]
        if context is not None:
            my_group.send(context)
            if img_list is not None:
                for list_i in img_list:
                    img_path = './img/' + list_i
                    try:
                        my_group.send_image(img_path)
                    except Exception as e:
                        print(e)
    elif type_ == 'friend':
        my_friend = bot.friends().search(name)[0]
        if context is not None:
            my_friend.send(context)
            if img_list is not None:
                for list_i in img_list:
                    img_path = './img/' + list_i
                    try:
                        my_friend.send_image(img_path)
                    except Exception as e:
                        print(e)
    else:
        print('Send type error !')


if __name__ == '__main__':

    # 测试发送微信消息
    context = 'hello , are you ok?'
    recv = cf.get('RECV_LIST', 'recv').split(',')
    for i in recv:
        type_ = i.split('-')[0]
        name = i.split('-')[1]
        send_by_wechat(type_, name, context)
