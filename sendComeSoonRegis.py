#!/usr/bin/python
# coding=utf-8

import sys
sys.path.append('./')
from byWechat import send_by_wechat
from getWhat import get_what
from mergeRegisData import MergeData
from byCsv import *
import os
import configparser as cparser

cfg_path = './receiverCfg.ini'
cf = cparser.ConfigParser()
cf.read(cfg_path, encoding='utf-8')

# 定时发送即将开始登记的项目情况
sts = '暂未开始'

csv_fp = 'csvData/cmSoonregis.csv'
cmsoon_regis = MergeData().merge_regis_data(sts)

# ---------by test------------------ #
# print(csv_fp)
if os.path.exists(csv_fp):
    print('csv:', read_csv(csv_fp, 1)[0], read_csv(csv_fp, 1)[-2])
    print('cmsoon:', cmsoon_regis[0], cmsoon_regis[-2])
else:
    print('%s is not exists, initializing ...' % csv_fp)
# ------------The End----------------#

if cmsoon_regis[0] == cmsoon_regis[-2] == '0':
    write_csv(csv_fp, cmsoon_regis)
else:
    if os.path.exists(csv_fp):
        if cmsoon_regis[0] != read_csv(csv_fp, 1)[0] or cmsoon_regis[-2] != read_csv(csv_fp, 1)[-2]:
            write_csv(csv_fp, cmsoon_regis)
            context = get_what(sts)
            recv = cf.get('RECV_LIST', 'recv').split(',')
            for i in recv:
                type_ = i.split('-')[0]
                name = i.split('-')[1]
                send_by_wechat(type_, name, context)
            print('Send Ok !')
    else:
        write_csv(csv_fp, cmsoon_regis)
        context = get_what(sts)
        recv = cf.get('RECV_LIST', 'recv').split(',')
        for i in recv:
            type_ = i.split('-')[0]
            name = i.split('-')[1]
            send_by_wechat(type_, name, context)
        print('Send Ok !')
