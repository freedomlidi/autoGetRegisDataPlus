#!/usr/bin/python
# coding=utf-8

import sys
sys.path.append('./')
from byWechat import send_by_wechat
from getWhat import get_what
import configparser as cparser
import os

cfg_path = './receiverCfg.ini'
cf = cparser.ConfigParser()
cf.read(cfg_path, encoding='utf-8')

# 定时发送正在登记的项目情况
sts = '正在登记'

# ------by test for not send wechat------- #
# print(get_what(sts))
# ------------the end--------------- #

# 运行前删除上次截图
old_png_lst = os.listdir('./img/')
if old_png_lst is not []:
    for old_png_lst_i in old_png_lst:
        os.remove('./img/' + old_png_lst_i)
# 爬取数据
context = get_what(sts)
new_png_lst = os.listdir('./img/')
print(new_png_lst)
recv = cf.get('RECV_LIST', 'recv').split(',')
for i in recv:
    type_ = i.split('-')[0]
    name = i.split('-')[1]
    send_by_wechat(type_, name, context, new_png_lst)
print('Send Ok !')
