# autoGetRegisDataPlus

#### 项目介绍
微信机器人，实时监控房管局网站以及定时爬取房源基本信息和登记详情，并将信息及时推送微信消息

#### 核心功能
1. 微信机器人，可按照输入的关键字回复特定消息，还可以尬聊
2. 机器人后台实时监控房管局网站，有新项目及时通知
3. 机器人后台定时爬取正在登记项目信息并推送微信消息（可推图文、文件）

#### 软件说明
基于Python3.6 + selenium + BeautifulSoup + phantomjs + wxpy


#### 安装教程

1. 需要提前安装对应的Python第三方库，如：bs4，selenium，phantomjs, wxpy等
2. 安装和jenkins构建基础见笔者文章：https://www.jianshu.com/p/a3693d1b7480 和 https://www.jianshu.com/p/b8464b7c4bcf

#### 使用说明

- **主程序说明** 
1. sendComeSoonRegis.py，为监控即将登记的项目数据
2. sendRegistering.py，为获取正在登记的项目数据
3. getRegistered.py，为获取结束登记的项目数据
4. replyMsg.py，为处理微信消息（runWeChat.py脚本已经废弃）

- **使用说明** 
1. 在linux环境下安装必要的库，并搭建jenkins环境
2. 第一步，通过命令：`nohup python -u replyMsg.py >replyMsg.log 2>&1 &`，运行replyMsg.py，并正常扫码登录微信，该脚本可以保持微信网页版持续在线
3. 在replyMsg.py运行的基础上，可按照自己需求分别将脚本sendComeSoonRegis.py和sendRegistering.py放在jenkins上，定时构建即可

#### 更新日志

- **autoGetRegisDataPlus V1.0** 
- **Date：2018.08.06**
- **Function & Modifies：**
1. 每天定时从房管局网站，爬取正在登记项目信息，并推送微信消息。爬取的信息包括：项目名称、总套数、登记时间、收资料时间、地址、和当前登记详情
2. 实时监控即将开始登记的楼盘，如有更新，第一时间爬取项目信息并推送微信消息
3. 爬取项目包括长安区和城六区，且类型可自由配置，例如：全部、正在登记、暂未开始和结束登记四种
4. 推送微信消息，可任意配置接收人/群聊
	
	
- **autoGetRegisDataPlus V1.1** 
- **Date：2018.08.17**
- **Function & Modifies：**
1. 增加项目价格字段显示
2. 实现微信网页版持续登录，基本实现7*24小时实时监控房管局官网动态
3. 每天11点和17点准时推送目前正在登记项目信息和当前登记人数，每隔15min监控房管局网站即将开始登记项目，有更新立即推送微信消息
3. 兼容Linux环境
4. 优化脚本，修复已知问题


- **autoGetRegisDataPlus V1.2**
- **Date：2018.09.02**
- **Function & Modifies：**
1. 增加接收人配置文件功能，只需要在receiverCfg.ini文件配置对应的接收人即可
2. 优化逻辑：暂未开始项目执行时追加新的项目价格，正在登记项目直接从xm_price.csv文件获取价格即可，不用重新爬取，缩短脚本执行时间
3. 修复已知问题


- **autoGetRegisDataPlus V1.3**
- **Date：2018.09.07**
- **Function & Modifies：**
1. 应群友@besos需求，增加当前登记人数截图功能
2. 修复当房管局网页无法访问时候，程序报错的bug
3. 增加/优化微信推送模块业务逻辑

#### [查看更多更新日志](https://gitee.com/freedomlidi/autoGetRegisDataPlus/releases)

