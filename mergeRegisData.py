#!/usr/bin/python
# coding=utf-8

# 长安区登记平台
# http://124.115.228.93/zfrgdjpt/xmgsca.aspx
# 非长安区登记平台
# http://124.115.228.93/zfrgdjpt/xmgs.aspx

from getRegisData import *
from getCurRegisData import GetCurRegis
from getPrice import GetPrice
import os


class MergeData:
    """
    合并数据
    1、城六区/长安区，正在登记项目的数据及详情
    2、城六区/长安区，即将开始登记项目数量及详情
    """
    def __init__(self):

        self.base_url = 'http://124.115.228.93/zfrgdjpt'

    def merge_regis_data(self, status):

        """
        城六区/长安区，全部/正在登记/暂未开始/登记结束 项目的数据及详情
        :return:
        """
        # 删除（初始化）价格xm_price.csv文件
        # if os.path.exists('csvData/xm_price.csv'):
        #     os.remove('csvData/xm_price.csv')

        # 初始化merge_data列表
        merge_data = []

        clq_url = self.base_url + '/xmgs.aspx'
        caq_url = self.base_url + '/xmgsca.aspx'

        if status == '全部':
            st = ''
        elif status == '正在登记':
            st = '?state=1'
        elif status == '暂未开始':
            st = '?state=2'
        elif status == '登记结束':
            st = '?state=4'
        else:
            st = ''

        lst = [clq_url, caq_url]
        for i in lst:
            regis_num = GetRegisData(i).get_num(status)
            merge_data.append(regis_num)
            if regis_num != '0':
                for j in GetRegisData(i + st).get_page_url():
                    merge_data.append(GetRegisData(j).get_data())
                    if status == '暂未开始':
                        GetPrice(j).get_price()
                    if status == '正在登记':
                        merge_data.append(GetCurRegis(j).get_cur_regis_data())
            else:
                merge_data.append('暂无项目')
        return merge_data



